//Declarar las dependencias siempre para ser utilizadas
//Ejm: require('DEPENDENCIA');
var express = require('express');
// Dependencia para uso de ficheros
var userFile = require('./user.json');
//como hemos utilizado el RAW en formato json utilizamos esta bliblioteca
//esta en node_module
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
//Declarar una variable para utilizar el objeto de la dependencia
//Ejm. VARDEPEND();
var app = express();
app.use(bodyParser.json()); // aqui dice que utilicemos el json

var totalUsers = 0;
//Variable contexto
const URL_BASE = '/apitechu/v1';
const URL_BASE_TECHU = '/apitechu/v2';
const PORT = process.env.PORT || 3004;


//Para llamar a un servicio es TIPO(URI,CALLBACK)
//Peticion GET de todos los 'users' (Collections)
app.get(URL_BASE + '/users',
function (request,response) {
  console.log('GET '+ URL_BASE + 'users');
  response.send(userFile);
});


//3000: puerto por defecto
app.listen(PORT,function(){
  console.log('Iniciando docker');
});
